/// <reference types="cypress" />

context('Tour', () => {
    beforeEach(() => {
      cy.visit('localhost:3000')
    })
  
    it('can load webpage', () => {
        cy.get('.MuiBox-root > .MuiTypography-root').should('have.text', 'Step 1: Choose an input ')
        cy.get('.MuiLinearProgress-root').should('have.attr', 'aria-valuenow').and('eq', '0')
        cy.get(':nth-child(2) > .MuiCardContent-root > .MuiBox-root > .MuiButtonBase-root').click()
        cy.get('.MuiLinearProgress-root').should('have.attr', 'aria-valuenow').and('eq', '17')
        cy.get('.MuiBox-root > .MuiTypography-root').should('have.text', 'Step 2: Choose a mini batch size ')
        cy.get(':nth-child(2) > .MuiCardContent-root > .MuiBox-root > .MuiButtonBase-root').click()
        cy.get('.MuiLinearProgress-root').should('have.attr', 'aria-valuenow').and('eq', '33')
        cy.get('.MuiBox-root > .MuiTypography-root').should('have.text', 'Step 3: Add first element to network ')
        cy.get(':nth-child(2) > .MuiCardContent-root > .MuiBox-root > .MuiButtonBase-root').click()
        cy.get('.MuiLinearProgress-root').should('have.attr', 'aria-valuenow').and('eq', '50')
        cy.get('.MuiBox-root > .MuiTypography-root').should('have.text', 'Step 4: Keep adding elements to network until network output meet classification dimensions ')
        cy.get(':nth-child(2) > .MuiCardContent-root > .MuiBox-root > .MuiButtonBase-root').click()
        cy.get('.MuiLinearProgress-root').should('have.attr', 'aria-valuenow').and('eq', '67')
        cy.get('.MuiBox-root > .MuiTypography-root').should('have.text', 'Step 5: Set an update momentum ')
        cy.get(':nth-child(2) > .MuiCardContent-root > .MuiBox-root > .MuiButtonBase-root').click()
        cy.get('.MuiLinearProgress-root').should('have.attr', 'aria-valuenow').and('eq', '83')
        cy.get('.MuiBox-root > .MuiTypography-root').should('have.text', 'Step 6: Save your Network Template ')
    })
})