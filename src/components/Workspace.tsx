import { Box, Button, Card, CardContent, CardHeader, Container, createStyles, makeStyles, Paper, TextField, Theme, Typography } from "@material-ui/core";
import React, { useState } from "react";
import Tour from "./Tour";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    section: {
      display: 'flex',
      flexWrap: 'wrap',
      alignContent: 'space-evenly',
      justifyContent: 'center',
      '& > *': {
          margin: theme.spacing(1),
          width: '40%'
      }
    },
    textField: {
      justifyContent: 'center',
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
      width: '25ch',
    },
    card: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center'
      },
    
  }),
);




function Workspace() {
    const classes = useStyles();
    
    const [expanded, setExpanded] = useState(false);
    const [activeStep, setActiveStep] = useState(1);

    function onClickExpand() {
        setExpanded(!expanded);
    }

    function incrementStep() {
        setActiveStep(Math.min(7, activeStep + 1));
    }

    function decrementStep() {
        setActiveStep(Math.max(1, activeStep - 1))
    }
    
    return (
        <React.Fragment>
        <Tour expanded={expanded} clickExpand={onClickExpand} activeStep={activeStep}/>
        <Container maxWidth="lg">
            <Paper className={classes.section}>
                <Card  variant="outlined" className={classes.card}>
                    <CardHeader title="MNIST" />
                    <CardContent>
                    <div>
                        <TextField id="MNIST-input-dimensions" 
                        label="Input Dimensions" 
                        value="28 x 28" 
                        className={classes.textField} 
                        disabled 
                        fullWidth 
                        variant="outlined"/>
                    </div>
                    <div>
                        <TextField id="MNIST-training-instances" 
                        label="Training Instances" 
                        value="60000" 
                        className={classes.textField} 
                        disabled 
                        fullWidth 
                        variant="outlined"/>
                    </div>
                    <div>
                        <TextField id="MNIST-testing-instances" 
                        label="Testing Instances" 
                        value="10000" 
                        className={classes.textField} 
                        disabled 
                        fullWidth 
                        variant="outlined"/>
                    </div>
                    <div>
                        <TextField id="MNIST-classifications" 
                        label="Classifications" 
                        value="10" 
                        className={classes.textField} 
                        disabled 
                        fullWidth 
                        variant="outlined"/>
                    </div>
                    <Box display='flex' justifyContent='center'>
                    <Button variant="contained" onClick={decrementStep}>
                        Select
                    </Button>
                    </Box>
                    
                    </CardContent>
                </Card>
                <Card  variant="outlined" className={classes.card}>
                    <CardHeader title="Faces" />
                    <CardContent>
                    <div>
                        <TextField id="Faces-input-dimensions" 
                        label="Input Dimensions" 
                        value="300 x 300" 
                        className={classes.textField} 
                        disabled 
                        fullWidth 
                        variant="outlined"/>
                    </div>
                    <div>
                        <TextField id="Faces-training-instances" 
                        label="Training Instances" 
                        value="500000" 
                        className={classes.textField} 
                        disabled 
                        fullWidth 
                        variant="outlined"/>
                    </div>
                    <div>
                        <TextField id="Faces-testing-instances" 
                        label="Testing Instances" 
                        value="100000" 
                        className={classes.textField} 
                        disabled 
                        fullWidth 
                        variant="outlined"/>
                    </div>
                    <div>
                        <TextField id="Faces-classifications" 
                        label="Classifications" 
                        value="2" 
                        className={classes.textField} 
                        disabled 
                        fullWidth 
                        variant="outlined"/>
                    </div>
                    <Box display='flex' justifyContent='center'>
                    <Button variant="contained" onClick={incrementStep} color='secondary'>
                        Select
                    </Button>
                    </Box>
                    </CardContent>
                </Card>
            </Paper>
        </Container>
        </React.Fragment>
        
    )
}

export default Workspace;