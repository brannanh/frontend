import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Grid, IconButton, LinearProgress, Paper } from '@material-ui/core';
import Step from './Step';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tour: {
        flexGrow: 1,
        marginBottom: theme.spacing(1),
        paddingLeft: theme.spacing(0),
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(0)
    },
    menuButton: {
        display: 'flex',
        justifyContent: 'flex-end',
        paddingRight: theme.spacing(3),
        alignSelf: 'center'
    },
    stepList: {
        paddingLeft: theme.spacing(1),
        alignContent: 'center',
    },
    progressBar: {
        paddingRight: theme.spacing(0),
        marginTop: theme.spacing(0)
    }
  }),
);

export default function Tour(props: {expanded: boolean, clickExpand: VoidFunction, activeStep: number}) { 
  const classes = useStyles();

  const steps = [
    <Step stepNumber={1} text='Choose an input' activeStep={props.activeStep}/>,
    <Step stepNumber={2} text='Choose a mini batch size' activeStep={props.activeStep}/>,
    <Step stepNumber={3} text='Add first element to network' activeStep={props.activeStep}/>,
    <Step stepNumber={4} text='Keep adding elements to network until network output meet classification dimensions' activeStep={props.activeStep}/>,
    <Step stepNumber={5} text='Set an update momentum' activeStep={props.activeStep}/>,
    <Step stepNumber={6} text='Save your Network Template' activeStep={props.activeStep}/>
    ];

  return (
    
    <Paper className={classes.tour} elevation = {3} square>
        <Grid container>
            <Grid item xs={11} className={classes.stepList}>
                {props.expanded ? steps : steps[Math.max(0, Math.min(props.activeStep - 1, steps.length - 1))]}
            </Grid>
            <Grid item xs={1} className={classes.menuButton}>
                <IconButton edge="end" color="inherit" aria-label="menu" size='small' onClick={props.clickExpand}>
                    {props.expanded ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                </IconButton>
            </Grid>
        </Grid>
        <Grid item xs={12} className={classes.progressBar}>
            <LinearProgress variant="determinate" value={Math.min(100, Math.max(0, 100*(props.activeStep-1)/6))} />
        </Grid>
    </Paper>
  );
}