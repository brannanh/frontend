import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { FormControlLabel, Switch } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }),
);

export default function TopBar(props: {onClick: VoidFunction, dark: boolean}) { 
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Pattrn
          </Typography>
          <FormControlLabel label={props.dark ? 'Dark Mode' : 'Light Mode'} control={<Switch
        checked={props.dark}
        onChange={props.onClick}
        color="secondary"
        name="checkedB"
        inputProps={{ 'aria-label': 'primary checkbox' }}
          />} />
        </Toolbar>
      </AppBar>
    </div>
  );
}