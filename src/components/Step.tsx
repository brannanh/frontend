import { Box, Typography } from '@material-ui/core';
import React from 'react';

export default function Step(props: { stepNumber: number; text: string; activeStep: number}) {
    return (
        <Box fontStyle={props.activeStep == props.stepNumber ? 'italic' : 'normal'}>
        <Typography variant={props.activeStep == props.stepNumber ? 'body1' : 'body2'} >
            Step {props.stepNumber}: {props.text} {<br />}
        </Typography>
        </Box>
    )
}