import React, { useState } from 'react';
import './App.css';
import { createMuiTheme, CssBaseline, PaletteType, ThemeProvider } from '@material-ui/core';
import Workspace from './components/Workspace';
import TopBar from './components/TopBar';
import { PaletteOptions } from '@material-ui/core/styles/createPalette';



function App() {

  const [lightOrDark, setLightOrDark] = useState<PaletteType>('dark');

  function invertLightness() {
    if (lightOrDark == 'dark') {
      setLightOrDark('light');
      return;
    }
    setLightOrDark('dark');
  }

  const theme = createMuiTheme({
    palette: {
      type: lightOrDark
    },
    typography: {
      fontFamily: [
        'JetBrainsMono',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
    },
  });
  return (
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <TopBar onClick={invertLightness} dark={lightOrDark == 'dark'}/>
        <Workspace />
      </ThemeProvider>
    </React.StrictMode>
  );
}

export default App;
